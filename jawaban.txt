Jawaban nomor 1
MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.025 sec)

MariaDB [(none)]>

Jawaban nomor 2
MariaDB [myshop]> create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.122 sec)

MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(8)       | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.025 sec)

MariaDB [myshop]> create table items(                      
    -> id int(8) auto_increment,                           
    -> name varchar(255),                                  
    -> description varchar(255),                           
    -> price int,                                          
    -> stock int,                                          
    -> categories_id int(8),                               
    -> primary key(id),                                    
    -> foreign key(categories_id) references categories(id)
    -> );                                                  
Query OK, 0 rows affected (0.252 sec)                      

MariaDB [myshop]> describe items;                                       
+---------------+--------------+------+-----+---------+----------------+
| Field         | Type         | Null | Key | Default | Extra          |
+---------------+--------------+------+-----+---------+----------------+
| id            | int(8)       | NO   | PRI | NULL    | auto_increment |
| name          | varchar(255) | YES  |     | NULL    |                |
| description   | varchar(255) | YES  |     | NULL    |                |
| price         | int(11)      | YES  |     | NULL    |                |
| stock         | int(11)      | YES  |     | NULL    |                |
| categories_id | int(8)       | YES  | MUL | NULL    |                |
+---------------+--------------+------+-----+---------+----------------+
6 rows in set (0.007 sec)                                               

MariaDB [myshop]> create table users(
    -> id int(8) auto_increment,     
    -> name varchar(255),            
    -> email varchar(255),           
    -> password varchar(255),        
    -> primary key(id)               
    -> );                            
Query OK, 0 rows affected (0.195 sec)

MariaDB [myshop]> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(8)       | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.022 sec)

Jawaban nomor 3
MariaDB [myshop]> insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.173 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+                           
| id | name    |                           
+----+---------+                           
|  1 | gadget  |                           
|  2 | cloth   |                           
|  3 | men     |                           
|  4 | women   |                           
|  5 | branded |                           
+----+---------+                           
5 rows in set (0.000 sec)                  

MariaDB [myshop]> insert into items(name, description, price, stock, categories_id) values("sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1),("uniklooh", "baju keren dari brand ternama", "500000", "50", 2),("imho watch", "jam tangan anak yang junjur banget", "2000000", "10", 1);
Query OK, 3 rows affected (0.049 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from items;                                                     
+----+-------------+------------------------------------+---------+-------+---------------+
| id | name        | description                        | price   | stock | categories_id |
+----+-------------+------------------------------------+---------+-------+---------------+
|  1 | sumsang b50 | hape keren dari merek sumsang      | 4000000 |   100 |             1 |
|  2 | uniklooh    | baju keren dari brand ternama      |  500000 |    50 |             2 |
|  3 | imho watch  | jam tangan anak yang junjur banget | 2000000 |    10 |             1 |
+----+-------------+------------------------------------+---------+-------+---------------+
3 rows in set (0.001 sec)                                                                  

MariaDB [myshop]> insert into users(name, email, password) values("john doe", "john@doe.com", "john123"),("jane doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.053 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | john doe | john@doe.com | john123   |
|  2 | jane doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

Jawaban nomor 4
a.
MariaDB [myshop]> select id, name, email from users;
+----+----------+--------------+                    
| id | name     | email        |                    
+----+----------+--------------+                    
|  1 | john doe | john@doe.com |                    
|  2 | jane doe | jane@doe.com |                    
+----+----------+--------------+                    
2 rows in set (0.001 sec)                           

b.
MariaDB [myshop]> select * from items where price > 1000000;
+----+-------------+------------------------------------+---------+-------+---------------+
| id | name        | description                        | price   | stock | categories_id |
+----+-------------+------------------------------------+---------+-------+---------------+
|  1 | sumsang b50 | hape keren dari merek sumsang      | 4000000 |   100 |             1 |
|  3 | imho watch  | jam tangan anak yang junjur banget | 2000000 |    10 |             1 |
+----+-------------+------------------------------------+---------+-------+---------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where name like 'uniklo%';                  
+----+----------+-------------------------------+--------+-------+---------------+
| id | name     | description                   | price  | stock | categories_id |
+----+----------+-------------------------------+--------+-------+---------------+
|  2 | uniklooh | baju keren dari brand ternama | 500000 |    50 |             2 |
+----+----------+-------------------------------+--------+-------+---------------+
1 row in set (0.112 sec)                                                          

MariaDB [myshop]> select items.id, items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items inner join categories on items.categories_id = categories.id;
+----+-------------+------------------------------------+---------+-------+---------------+--------+
| id | name        | description                        | price   | stock | categories_id | name   |
+----+-------------+------------------------------------+---------+-------+---------------+--------+
|  1 | sumsang b50 | hape keren dari merek sumsang      | 4000000 |   100 |             1 | gadget |
|  2 | uniklooh    | baju keren dari brand ternama      |  500000 |    50 |             2 | cloth  |
|  3 | imho watch  | jam tangan anak yang junjur banget | 2000000 |    10 |             1 | gadget |
+----+-------------+------------------------------------+---------+-------+---------------+--------+
3 rows in set (0.190 sec)

jawaban nomor 5
MariaDB [myshop]> update items set price = "2500000" where id = 1;                         
Query OK, 1 row affected (0.911 sec)                                                       
Rows matched: 1  Changed: 1  Warnings: 0                                                   
                                                                                           
MariaDB [myshop]> select * from items;                                                     
+----+-------------+------------------------------------+---------+-------+---------------+
| id | name        | description                        | price   | stock | categories_id |
+----+-------------+------------------------------------+---------+-------+---------------+
|  1 | sumsang b50 | hape keren dari merek sumsang      | 2500000 |   100 |             1 |
|  2 | uniklooh    | baju keren dari brand ternama      |  500000 |    50 |             2 |
|  3 | imho watch  | jam tangan anak yang junjur banget | 2000000 |    10 |             1 |
+----+-------------+------------------------------------+---------+-------+---------------+
3 rows in set (0.000 sec)                                                                  